---
layout: page
current: encuestas
title: Encuestas
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
---

### Resultados de la encuesta preferencias de lectura

- 28 de 44 estudiantes contestaron la encuesta.
- Al 64,3% les gusta leer regularmente, mientras que al 10,7% no le gusta leer casi nada.  

![¿Te gusta leer?]({{ "/assets/images/blog-content/encuesta1.png" | relative_url }})

Cantidad de tiempo para leer:

![¿Te gusta leer?]({{ "/assets/images/blog-content/encuesta2.png" | relative_url }})

¿Cuáles son tus tres libros favoritos?

- Muchos han leído El Principito
- Otros el Diario de Ana Frank
- El alquimista 
- IT de Sthephen King
- Leyendas de mexico
- Don Quijote de la Mancha
- La vuelta al mundo en 80 días

¿Cuáles son tus tres autores favoritos?

En resumen:

- John Green 
- CS. LEWIS
- Julio Verne 
- Mario Benedetti, Oscar Wilde, Edgar Alan Poe 

Tipos de lectura:

![¿Te gusta leer?]({{ "/assets/images/blog-content/encuesta3.png" | relative_url }})

- Cómics, sucesos de la vida, históricos narrativos,
- De amor, romance
- Drama, suspenso
- Ficción
- Misterio, terror, terror realista, leyendas
- Comedia 
- Medicina, ciencia 
- Animales

Expectativas del club:

- Se espera que este club sea divertido, se enfatiza la diversión y que se mejore su creatividad
- Leer diferentes libros de interés y leer más
- Aprender a escribir, por ejemplo un libro, historias, sucesos, autobiografías, resúmenes
- Aprender a narrar y redactar
- Mejorar la ortografía, letra, manejo de lenguaje y el vocabulario
- Reforzar la materia de español

### Ya no se aceptan más respuestas en las encuestas.

- [Datos de contacto](https://goo.gl/forms/AGEGtu2SeeJMBm503){:target="_blank"}. En ésta encuesta se solicita tu nombre completo y un correo electrónico de cualquier tipo, en donde (_ocasionalmente_) puedas recibir y leer notificaciones exclusivamente del Club.
- [Preferencias de lectura](https://goo.gl/forms/SiwlZDtP04JAFVo93){:target="_blank"}. Aquí podrás responder sobre tus preferencias lectoras, los temas que son de tu agrado, intereses y expectativas del Club.


Última actualización: martes, 9 de octubre.
{:class="text-xl"}