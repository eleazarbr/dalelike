---
layout: page
current: noticias
title: Noticias
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
---

### Tareas

1. ~~Contestar las encuestas.~~
2. ~~Realizar actividad de la Anécdota hasta el paso 8.~~
3. ~~Enviar un avance de tu anécdota por cualquiera de los medios de comunicación mencionados arriba o en la clase del viernes, 5 de octubre.~~
4. 


### Resultados de encuestas

Haz clic en el siguiente enlace para ver los resultados de las encuestas.

- <a target="_blank" href="{{ site.baseurl }}encuestas/">Resultados de encuestas</a>

### Medios de comunicación 

De acuerdo a sus preferencias y votación en el grupo, los siguientes son los medios de comunicación para compartir y facilitar información relacionada con el club Dale Like a tu Escritura.

- Grupo de Facebook: [facebook.com/groups/dalelikeclub](https://www.facebook.com/groups/dalelikeclub/)
- Grupo de Whatsapp: [Haz clic para unirte](https://chat.whatsapp.com/J0LW5E0lY0S26JdlxG4B9D)
- Correo electrónico: [eleazar.resendez@outlook.com](mailto:eleazar.resendez@outlook.com) 


Última actualización: miércoles, 10 de octubre.
{:class="text-xl"}