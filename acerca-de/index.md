---
layout: page
current: about
title: Sobre el Club
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
---

> **Tabla de contenido**
- [Contenido del Club](#contenido-del-club)
- [Aprendizajes esperados](#aprendizajes-esperados)
- [Evaluación y niveles de desempeño](#evaluaci%C3%B3n-y-niveles-de-desempe%C3%B1o)


Este club está dirigido a los estudiantes interesados en perfeccionar tanto su _expresión escrita_ como la _correcta redacción_ de varios tipos de textos considerando:

- Redacción y estilo. 
- Ortografía y puntuación. 
- Estructuración de las ideas. 
- Presentación del texto. 
- Revisión de los textos.

El propósito es despertar el interés atrayéndolos para vivir esta experiencia e iniciarse en el mundo de la escritura creativa.

### Contenido del Club

Los contenidos que se abordarán durante el Club de Lectura y Escritura Creativa. Dale “Like” a Tu Escritura serán:

1. La anécdota
2. La autobiografía
3. El diario personal 
4. La historieta
5. La reseña
6. El cuento
7. Los textos poéticos 
8. Las obras teatrales

### Aprendizajes esperados

- Selecciona datos y sucesos más importantes de la vida, para **relatar una anécdota**.
- Utiliza adecuadamente recursos lingüísticos, modos y tiempos verbales en la **redacción de su autobiografía**.
- Utiliza adecuadamente recursos lingüísticos, modos y tiempos verbales en la **redacción de su Diario Personal**.
- Emplea los recursos gráficos y visuales para la construcción de un texto. Elige **la reseña** como recurso para difundir obras, libros, películas. Escribe **un cuento** con propósitos expresivos y estéticos.
- Escribe **poemas** tomando otros como referencia.
- Desarrolla su capacidad para elaborar **guiones literarios**.

### Evaluación y niveles de desempeño

1. **Nivel IV** (N-IV). Indica dominio sobresaliente de los aprendizajes. Este Nivel de Desempeño se asocia con una calificación aprobatoria de 10.
2. **Nivel III** (N-III). Indica dominio satisfactorio de los aprendizajes. Este Nivel de Desempeño se asocia con las calificaciones aprobatorias de 8 y 9.
3. **Nivel II** (N-II). Indica dominio básico de los aprendizajes. Este Nivel de Desempeño se asocia con las calificaciones aprobatorias de 6 y 7.
4. **Nivel I** (N-I). Indica dominio insuficiente de los aprendizajes. Este Nivel de Desempeño se asocia con una calificación reprobatoria de 5.

**Criterios de evaluación**

|Criterio|Puntuación|
|---|---|
|Conocimientos|20%|
|Habilidad|50%|
|Actitud/valores|30%|

**Criterios de evaluación de sus trabajos finales**

1. Redacción
2. Ortografía y signos de puntuación
3. Estructuración de las ideas
4. Presentación del texto
5. Tiempos de entrega

Última actualización: miércoles, 10 de octubre.
{:class="text-xl"}