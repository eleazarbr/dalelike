---
layout: page
current: recursos
title: Recursos
navigation: true
logo: 'assets/images/ghost.png'
class: page-template
subclass: 'post page'
---

### Material de apoyo

- Requisitos de presentación que debe reunir el trabajo escrito de la autobiografía.

1. Debe contar con **TODOS** los puntos del **organizador de ideas**. 
2. El trabajo se debe de entregar escrito a mano o en computadora.
    1. Si tu trabajo es escrito a mano debes cuidar **escribir con letra clara y legible, las faltas de ortografía restan puntos**.
3. Fecha de entrega: **martes, 30 de octubre**

Otros consejos:

- Se valorará la presentación. Si añades dibujos, gráficos, una portada atractiva, etc. le darás otro aire al trabajo. No te limites sólo a escribir los contenidos. Tienes que "adornarlo". 

Ejemplo simple:

![Autobiografia]({{ "/assets/images/blog-content/bio-sample.png" | relative_url }})


- Videos de biografías:

https://www.youtube.com/watch?v=m7oU9Cq-HwM


Última actualización: jueves, 25 de octubre.
{:class="text-xl"}