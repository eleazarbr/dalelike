---
layout: post
current: post
cover: assets/images/cover/anec-intro.jpg
navigation: true
title: "La anécdota - Introducción"
description: 
date: 2018-10-03
tags: [anecdotas]
class: post-template
subclass: 'post'
author: eleazar
permalink: /anecdota-introduccion.html
---

Una anécdota es un relato breve de un hecho curioso o divertido; se emplea sobre todo en conversaciones, aunque también puede adoptar la forma de un texto escrito.

### Breve explicación

La anécdota siempre cuenta sucesos que ostentan interés o llaman la atención por su singularidad, y _casi siempre es el protagonista de ellos quien las narra_ luego de vivirlas.

Normalmente está basada en _hechos reales_, que tienen lugar en ambientes también reales. Los protagonistas de este tipo de narraciones suelen tener una participación directa o indirecta sobre lo que se cuenta.

### Estructura de la anécdota

La estructura de la anécdota es como cualquier otro relato:

![Estructura anécdota]({{ "/assets/images/blog-content/anec-1.png" | relative_url }})

### Puntos importantes

- La misión de una anécdota es la transmisión de un suceso que se vivió y es potestad de quien la narra saberle aportar una cuota especial de realidad y emoción para despertar en los otros, en quienes la oyen, empatía.
- Cabe destacarse que si bien cualquier persona puede protagonizar y contar anécdotas, hay individuos que ostentan una disposición especial para hacerlo, como sucede con los chistes y el humor, saben contarlos de una manera tan entretenida que por caso saben captar una mejor empatía, que aquellos que no presentan esta inclinación natural.
- Para hacer de una anécdota un hecho singular es necesario seguir una serie de parámetros o de trucos que ayuden en ese impacto que se busca muchas veces a la hora de transmitir, por ejemplo **generar suspenso**, es una excelente alternativa, porque claro, se le anticipan de a poco los sucesos, logrando engancharlo de a poco y cada vez más en la historia, y así se quedará escuchando hasta el final.

También es muy importante mantener un orden coherente a la hora de relatar la anécdota, esto suma no solamente en la atención sino también en la comprensión.

Las anécdotas **son importantes porque** ayudan a desarrollar la _creatividad_ y la _capacidad expresiva al narrarlas_, ya que requieren de recursos literarios en su elaboración.

### Ejemplos de anécdotas

#### La puerta del cielo... sobre todo para los judíos

Entre 1943 y 1944 se rodó en Roma "La puerta del cielo", película dirigida por Vittorio de Sica y producida por el Vaticano, en la que se contaba el viaje de varios peregrinos a Loreto. 

En realidad, la propia película en sí era lo que menos les interesaba a todos: era una simple tapadera para poder salvar a 300 judíos y antifascistas de la muerte. Para conseguirlo, se dijo que eran técnicos y extras y se les alojó en la Basílica de san Pablo Extramuros, donde se realizó la grabación.

La grabación se alargó justo hasta el día antes a la llegada de los aliados, lo necesario para salvar a cientos de personas. La película, en cambio, pasó sin pena ni gloria.

#### Reina después de muerta

La noble gallega Inés de Castro nunca llegó a ser reina de Portugal... en vida. Amante de Pedro de Portugal, era rechazada tanto por su padre, Alfonso IV, como por los nobles. Los amantes se casaron en secreto, pero para impedir que llegase a reinar, varios nobles la asesinaron en 1355. 

Dos años después, Pedro subió al trono y, según cuenta la leyenda, exhumó el cadáver de su amada para que pudiera ser coronada. En cualquier caso, Inés está considerada reina de Portugal y fue enterrada en el monasterio de Alcobaça junto a la sepultura de Pedro.

#### El manco de Lepanto... no era manco

Los detractores de Miguel de Cervantes le pusieron el sobrenombre de "El manco de Lepanto", que ha llegado hasta nuestros días. Sin embargo, Cervantes conservó toda su vida los dos brazos, solo que perdió parte de la movilidad del izquierdo como consecuencia de las heridas sufridas durante la famosa batalla de 1571.

<hr>

> Al finalizar la [actividad]({{ site.baseurl }}anecdota-actividad.html), le darás "**Like**" (me gusta) a tu trabajo, con el propósito de despertar tu interés de seguir escribiendo.

Última actualización: miércoles, 3 de octubre.
{:class="text-xl"}