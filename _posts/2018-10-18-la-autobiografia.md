---
layout: post
current: post
cover: assets/images/cover/biography.jpg
navigation: true
title: "¿Qué es la autobiografía?"
description: 
date: 2018-10-18
tags: [autobiografia]
class: post-template
subclass: 'post'
author: eleazar
permalink: /autobiografia-introduccion.html
---

La autobiografía es el relato de la vida de una persona escrita por ella misma.

Es una **obra personal** ya que es el propio autor el encargado de expresar los pormenores de uno o varios aspectos de su vida. 

Es un tipo de redacción típicamente literario.

Lo narrado en ella es **verídico** ya que se trata de la vida del autor escrita por él mismo.

El contenido es variado, así como lo son las vivencias que cualquier ser humano tiene durante su vida; tradicionalmente, una persona en su autobiografía relatará _todo aquello que le haya sucedido desde su nacimiento hasta el momento en el que se ha puesto a escribir su autobiografía_. 

- Los primeros años de vida
- La composición de su familia, 
- Logros, 
- Fracasos, 
- Estudios, 
- Viajes
- Experiencias inolvidables.

Se encuentra redactada generalmente en **primera persona**. 

En la misma **no hay ficción**, todo lo que se relata es _real_, ha sucedido y por ello es de un interés especial.

La práctica de la escritura autobiográfica nos revela aspectos nuestros que desconocíamos.

Escribir sobre uno mismo no es tan sencillo como podría parecer.

La autobiografía tiene algo de otros géneros como la biografía, las memorias y el diario personal (íntimo) se distingue de ellos por varias cuestiones.

- De la biografía se diferencia especialmente por la _identidad entre narrador y protagonista_ del relato que no sucede en el caso de la biografía.
- Respecto de las memorias, se distancia de esta en cuanto a que el _acento está puesto en la vida privada de quien asume el rol de narrador_, en tanto, las memorias centran su interés especialmente en los acontecimientos externos de la vida del protagonista.
- Del diario personal (íntimo) se distancia porque la autobiografía es una **retrospectiva de la vida del narrador**, es decir, _mucho tiempo ha pasado de aquello que narra_, en cambio, el diario personal supone una escritura _paralela_ a la sucesión de los hechos que se escriben.

Aunque en la historia existen ejemplos, el género autobiográfico ha logrado una enorme difusión más que nada en estos tiempos que corren, especialmente porque se ha convertido en el predilecto de aquellos **personajes célebres** que hacen uso del mismo para **dar cuenta de sus experiencias de vida a sus seguidores, ávidos por descubrirlas**.

En la actualidad se considera un _género exitoso_. 

En tanto, a partir del interés que este tipo de lecturas ha despertado en el público, la industria editorial ha sabido construir alrededor de las autobiografías un muy redituable negocio. 

En las últimas décadas la autobiografía pasó a ser considerada como un género literario y casi todas sus propuestas se convierten de inmediato **best-sellers**.

Si debiéramos ahondar en la causa de esto último y brindar una interpretación, seguramente, esto está en estrecha vinculación con el **afán del ser humano** de _conocer más y más_, en especial lo **íntimo**, de aquellas personalidades rutilantes de algún ámbito, de conocerlas en su intimidad y a través de este conocimiento _poder identificarse_ con ellas, porque claro, no dejan de ser humanos a los que les pasan las mismas cosas que a todos.

Tampoco podemos soslayar que muchas de estas historias escritas en primera persona que se convierten en éxito suelen ser adaptadas a la _pantalla grande_.

Última actualización: miércoles, 18 de octubre.
{:class="text-xl"}