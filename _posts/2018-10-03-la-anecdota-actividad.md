---
layout: post
current: post
cover: assets/images/cover/activity.jpg
navigation: true
title: "La anécdota - Actividad"
description: 
date: 2018-10-03
tags: [anecdotas]
class: post-template
subclass: 'post'
author: eleazar
permalink: /anecdota-actividad.html
---

El propósito y aprendizaje esperado de esta actividad es: selecciona datos y sucesos más importantes de la vida, para relatar una anécdota.

- **Competencia que favorece**: Emplea el lenguaje para comunicarse y como instrumento para seguir aprendiendo.
- **Materiales**: Cuaderno, lápiz, plumas, hojas blancas.

<!-- Si tienes dudas, puedes dejar un comentario en la parte de abajo.
{:class="text-red"} -->

1. Realizar en plenaria conversaciones sencillas, para contar hechos de su vida.
2. Pensar en algún suceso que hayan vivido que se destaque por ser gracioso, tragicómico, divertido, etc. Y que estén dispuestos a contar en público, sin avergonzarse.
3. Escribir en una hoja, los acontecimientos principales del suceso, en el orden en que ocurrieron.
4. Ensayar la forma en que lo contarán, fijándose principalmente en destacar los ámbitos más interesantes y divertidos de la historia.
5. Distribuir lo ocurrido en tres párrafos de acuerdo con la estructura siguiente:
¿**A quién le ocurrió**?, ¿**Cuándo ocurrió**?, ¿**Dónde ocurrió**?, ¿**Qué sucedió**?, ¿**Qué ocurrió luego**?, ¿**En qué quedó todo**?, ¿**Qué consecuencias tuvo**?
6. Considerar al inicio expresiones como: “un día”; “el otro día”; “hace unos años”; “una noche”.
7. Introducir otras expresiones que marquen la linealidad temporal de los acontecimientos como: “luego”; “después”; “a continuación”.
8. Hacer una breve referencia al espacio en el que ocurren los hechos que permita al interlocutor “situarse” en sus hechos.
9. Emplear un punto de vista narrativo: utilizando la 1a persona en las formas verbales.
10. Combinar la 1a y 3a personas cuando se es el protagonista, pero además participan más personas.
11. Emplear verbos de acción y movimiento para dar la sensación de que los hechos transcurren rápidamente.
12. Evitar descripciones minuciosas que puedan hacer perder el interés por lo que se cuenta.
13. Cuidar los requisitos de presentación que debe reunir un trabajo escrito.
14. Cuidar la legibilidad de la letra, el uso de mayúsculas, signos de puntuación y reglas de ortografía.
15. Revisar y corregir de manera continua el avance de lo escrito.
16. Relatar la anécdota ante sus compañeros en el aula.
17. Concluir la actividad presentando al maestro(a) el escrito a **manuscrito** o **computadora** contemplando todos sus datos de grado.
18. Dar “**like**” y valorar el texto escrito en el club.
19. Participar en la selección de los mejores trabajos para publicarlos en el periódico mural.
20. Iniciar el portafolio de evidencias del club, ir incluyendo las producciones.

Última actualización: jueves, 4 de octubre.
{:class="text-xl"}