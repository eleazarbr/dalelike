---
layout: post
current: post
cover: assets/images/cover/activity.jpg
navigation: true
title: "Actividad - La autobiografía"
description: 
date: 2018-10-18
tags: [autobiografia]
class: post-template
subclass: 'post'
author: eleazar
permalink: /autobiografia-actividad.html
---

El propósito y aprendizaje esperado de esta actividad es: Utiliza adecuadamente recursos lingüísticos, modos y tiempos verbales en la redacción de tu autobiografía.

- **Competencia que favorece**: Emplea el lenguaje para comunicarse y como instrumento para seguir aprendiendo.
- **Materiales**: Cuaderno, lápiz, plumas, hojas blancas.

1. Observar y contestar un organizador de ideas.
2. Comprender que a escribir **sólo se aprende escribiendo**.
3. Relatar su vida apoyándose en el contenido del organizador.
4. **Escribir para conocerse**, para saber quiénes son, para describirse.
5. Elegir el lenguaje de una forma libre y enriquecedora.
6. Sentir ser autor y protagonista de la propia historia.
7. Narrar los hechos de su vida en orden cronológico.
8. Reconstruir a través de esquemas o líneas del tiempo para recuperar los datos más
relevantes, **hechos o personas que influyeron en su vida**.
9. Emplear un punto de vista narrativo: utilizando la 1a persona en las formas verbales.
10. Utilizar adecuadamente los modos y tiempos verbales.
11. Cuidar los requisitos de presentación que debe reunir un trabajo escrito.
12. **Cuidar la legibilidad de la letra, el uso de mayúsculas, signos de puntuación y reglas de ortografía**.
13. Revisar de manera continua el avance de lo escrito.
14. Elaborar borradores que cumplan con las características de éste tipo de texto. 
15. Leer la autobiografía ante sus compañeros en el aula.
16. Concluir la actividad presentando al maestro (a) el escrito a manuscrito o computadora contemplando todos los datos de grado. 
17. Dar “Like” y valorar el texto escrito en el club.
<!-- 18. Participar en la selección de los mejores trabajos para publicarlos en el periódico mural. -->
<!-- 19. Incluir en el portafolio de evidencias el trabajo. -->

Última actualización: miércoles, 18 de octubre.
{:class="text-xl"}